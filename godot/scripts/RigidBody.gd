extends RigidBody
#var lastmousepos= Vector2()
const MovementForce=0.2
const HMovementForce=0.1
const AirFriction=0.2
const RotationSpeed=0.01

func _integrate_forces(state):

	#var mousepos=get_viewport().get_mouse_position()
	# print(mousepos)
	#var mousespeed=(mousepos-lastmousepos)/state.step
	# print(mousespeed)
	#lastmousepos=mousepos
	var Up = Vector3(0,1,0)
	var Left = Vector3(0,0,-1)
	var Right = Vector3(0,0,1)
	#state.angular_velocity=Vector3(0,mousespeed.x*RotationSpeed,mousespeed.y*RotationSpeed)
	# print (state.angular_velocity)
	
	
	if Input.is_key_pressed(KEY_W):
		#print("key pressed")
		#print (Forward)
		state.apply_impulse(state.center_of_mass,Up*MovementForce)
	if Input.is_key_pressed(KEY_A):
		#print("key pressed")
		#print (Forward)
		state.apply_impulse(state.center_of_mass,Left*HMovementForce)
	if Input.is_key_pressed(KEY_D):
		#print("key pressed")
		#print (Forward)
		state.apply_impulse(state.center_of_mass,Right*HMovementForce)
func _ready():
#	lastmousepos=get_viewport().get_mouse_position()
	pass